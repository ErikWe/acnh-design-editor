package gui.panels;

import design.DesignEditor;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class PreviewPanel implements Panel {
    private final JLabel imageLabel = new JLabel();

    public PreviewPanel() {
        DesignEditor.getDesignEditorInstance().addDisplayPanel(this);
    }

    @Override
    public JPanel get() {
        MigLayout displayLayout = new MigLayout("wrap 1");
        JPanel displayPanel = new JPanel(displayLayout);
        reloadImage();
        this.imageLabel.setMinimumSize(new Dimension(500, 500));
        displayPanel.add(imageLabel);
        return displayPanel;
    }

    public void reloadImage() {
        BufferedImage bufferedImage = DesignEditor.getDesignEditorInstance().getImage();
        if (bufferedImage != null) {
            Image scaledInstance = bufferedImage.getScaledInstance(500, 500, Image.SCALE_DEFAULT);
            this.imageLabel.setIcon(new ImageIcon(scaledInstance));
        }
    }
}
