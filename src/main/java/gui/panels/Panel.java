package gui.panels;

import javax.swing.*;

public interface Panel {

    public JPanel get();
}
