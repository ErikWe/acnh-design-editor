package gui.panels;

import design.DesignEditor;
import net.miginfocom.swing.MigLayout;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionListener;
import java.io.File;

public class ControlPanel implements Panel {

    private static final Logger logger = LogManager.getLogger(ControlPanel.class);

    public ControlPanel() {}

    @Override
    public JPanel get() {
        MigLayout controlLayout = new MigLayout("wrap 3");
        JPanel controlPanel = new JPanel(controlLayout);
        JButton button_read = new JButton("Read Img");
        button_read.addActionListener(getActionListenerButtonRead());
        JButton button_shrink = new JButton("Shrink Img");
        button_shrink.addActionListener(getActionListenerButtonShrink());
        JButton button_write = new JButton("Write Img");
        button_write.addActionListener(getActionListenerButtonWrite());

        controlPanel.add(button_read);
        controlPanel.add(button_shrink);
        controlPanel.add(button_write);

        return controlPanel;
    }

    private ActionListener getActionListenerButtonShrink() {
        logger.info("Clicked button shrink");
        return e -> DesignEditor.getDesignEditorInstance().shrinkImage(32,32);
    }

    private ActionListener getActionListenerButtonRead() {
        logger.info("Clicked button read");
        return e -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File(System.getProperty("user.home") + "/Desktop"));
            FileNameExtensionFilter filter = new FileNameExtensionFilter("PNG Image", "png");
            chooser.setAcceptAllFileFilterUsed(false);
            chooser.addChoosableFileFilter(filter);

            int returnValue = chooser.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File selectedFile = chooser.getSelectedFile();
                DesignEditor.getDesignEditorInstance().readImage(selectedFile.getAbsolutePath());
            }
        };
    }

    private ActionListener getActionListenerButtonWrite() {
        logger.info("Clicked button write");
        return e -> DesignEditor.getDesignEditorInstance().writeImage(null);
    }
}
