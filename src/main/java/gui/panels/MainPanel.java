package gui.panels;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;

public class MainPanel implements Panel {

    public MainPanel() {}

    @Override
    public JPanel get() {
        MigLayout mainLayout = new MigLayout("wrap 1");
        JPanel mainPanel = new JPanel(mainLayout);
        mainPanel.add(new HeaderPanel().get());
        mainPanel.add(new PreviewPanel().get());
        mainPanel.add(new ControlPanel().get(),"align 50% 50%");
        return mainPanel;
    }
}
