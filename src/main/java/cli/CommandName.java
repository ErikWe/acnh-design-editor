package cli;

public enum CommandName {
    EXIT("exit", "Closes the program",true),
    HELP("help","Prints all available commands", true),
    READ("read","Reads an image from a given filepath", true),
    WRITE("write","Writes the current image to a given path or overwrites the original image", true),
    SHRINK("shrink", "Shrinks the current image to a given size or 16x16 px by default", true),
    UNKNOWN("unknown","Placeholder for invalid commands", false);

    public final String name;
    public final String description;
    public final boolean mainCommand;
    private CommandName(String name, String description, boolean mainCommand) {
        this.name = name;
        this.description = description;
        this.mainCommand = mainCommand;
    }

    public static CommandName fromString(String text) {
        for (CommandName commandName : CommandName.values()) {
            if (commandName.name.equalsIgnoreCase(text)) {
                return commandName;
            }
        }
        return UNKNOWN;
    }
}
