package cli.commands;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Command_Exit implements Command {
    private static final Logger logger = LogManager.getLogger(Command_Exit.class);
    private static final Option option_help = new Option("h", "help", false, "Lists available arguments");
    private static final Option option_save = new Option("s", "save", false, "Saves all changes before exiting the program");

    @Override
    public Options getCommandOptions() {
        Options options = new Options();
        options.addOption(option_help);
        options.addOption(option_save);
        return options;
    }

    @Override
    public void executeCommand(String[] args) {
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(getCommandOptions(), args);
            if (line.hasOption(option_save)) {
                logger.info("Called EXIT with option --save");
                //TODO perform save exit
                exitProgram();
            } else if (line.hasOption(option_help)) {
                logger.info("Called EXIT with option --help");
                describeCommandOptions();
            } else {
                logger.info("Called EXIT without any option");
                exitProgram();
            }
        } catch (ParseException exception) {
            System.err.println(exception.getMessage());
        }
    }

    @Override
    public void describeCommandOptions() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("exit", getCommandOptions());
    }

    private void exitProgram() {
        System.exit(0);
    }
}
