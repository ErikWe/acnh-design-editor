package cli.commands;

import cli.CommandName;
import design.DesignEditor;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Command_Shrink implements Command {

    private static final Logger logger = LogManager.getLogger(Command_Shrink.class);
    private static final Option option_help = new Option("h", "help", false, "Lists available arguments");
    private static final Option option_width = new Option("w", "width", true, "Target width in px of shrink process");
    private static final Option option_length = new Option("l", "length", true, "Target length in px of shrink process");
    @Override
    public Options getCommandOptions() {
        Options options = new Options();
        options.addOption(option_help);
        options.addOption(option_width);
        options.addOption(option_length);
        return options;
    }

    @Override
    public void executeCommand(String[] args) {
        int width = 16;
        int length = 16;
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(getCommandOptions(), args);
            if (line.hasOption(option_help)) {
                logger.info("Called SHRINK with option --help");
                this.describeCommandOptions();
            } else {
                if (line.hasOption(option_width)) {
                    logger.info("Called SHRINK with option --width");
                    width = Integer.parseInt(line.getOptionValue(option_width));
                }

                if (line.hasOption(option_length)) {
                    logger.info("Called SHRINK with option --length");
                    length = Integer.parseInt(line.getOptionValue(option_length));
                }

                logger.info(String.format("Perform SHRINK with width: %dpx and length: %dpx", width, length));
                DesignEditor.getDesignEditorInstance().shrinkImage(width, length);
            }
        } catch (ParseException exception) {
            System.err.println(exception.getMessage());
        }

    }

    @Override
    public void describeCommandOptions() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("shrink", getCommandOptions());
    }
}
