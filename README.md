# ACNH-Design-Editor
The goal of this application is to provide a tool which helps to create more difficult designs in Animal Crossing New Horizons.

## Content
* Requirements
* Build/Package
* Usage
* License

## Requirements
To be able to build and package the project in the next section, make sure you have the following requirements set-up:
* Java 17
* Maven

## Build/Package
The project is intended to be packaged and shipped as a *.jar* file. To package it, open the project's root folder and perform the `mvn package` command. This will create two *.jar* files inside the *target* folder.
One with and one without the dependencies. For testing or using the project, use the *acnh-design-editor-{VERSION}-jar-with-dependencies.jar*.

If you experience any troubles, make sure that the correct version of java is used `java -version` and that Maven is installed and setup correctly. Maybe checkout if there is a [newer version](https://maven.apache.org/install.html) of Maven and update it if possible.

## Usage
Currently, there are two modes which are supported. The CLI mode for operation in you command line and the GUI mode which offers a window for controls and preview.

### CLI Mode
Start the program in CLI mode: `java -jar acnh-design-editor-{VERSION}-jar-with-dependencies.jar`

Use the command `help` to get a list of all possible commands. In addition, each individual command offers a `-h/--help` parameter to display the command options.

### GUI Mode
Start the program in GUI mode: `java -jar acnh-design-editor-{VERSION}-jar-with-dependencies.jar --gui`

The interface itself should then be self-explanatory.

## License
ACNH-Design-Editor Copyright (C) 2022 ErikWe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/.
